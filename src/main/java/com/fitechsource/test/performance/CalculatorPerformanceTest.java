package com.fitechsource.test.performance;

import com.fitechsource.test.TestCalc;
import com.fitechsource.test.TestConsts;
import com.fitechsource.test.TestException;
import com.fitechsource.test.util.Calculator;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.Set;

public class CalculatorPerformanceTest {

    public static void main(String[] args) {
        try (BufferedWriter bufferedWriter =
                     new BufferedWriter(
                             new OutputStreamWriter(new FileOutputStream("CalculatorPerformanceTest.txt")))) {
            bufferedWriter.write("----------------------");
            bufferedWriter.newLine();
            bufferedWriter.write("Using single thread: " + calculateSingleThread());
            bufferedWriter.newLine();
            bufferedWriter.write("----------------------");
            bufferedWriter.newLine();
            bufferedWriter.write("Using multithreading: " + calculateMultiThread());
        } catch (IOException | TestException e) {
            e.printStackTrace();
        }
    }

    public static long calculateSingleThread() throws TestException {
        long start = System.currentTimeMillis();
        Set<Double> res = new HashSet<>();
        for (int i = 0; i < TestConsts.N; i++) {
            res.addAll(TestCalc.calculate(i));
        }
        return System.currentTimeMillis() - start;
    }

    public static long calculateMultiThread() {
        long start = System.currentTimeMillis();
        Calculator.calculate();
        return System.currentTimeMillis() - start;
    }

}
