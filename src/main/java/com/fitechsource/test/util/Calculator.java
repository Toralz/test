package com.fitechsource.test.util;

import com.fitechsource.test.TestCalc;
import com.fitechsource.test.TestException;

import java.util.HashSet;
import java.util.Set;

import static com.fitechsource.test.TestConsts.MAX_THREADS;
import static com.fitechsource.test.TestConsts.N;
import static java.lang.Math.round;

public class Calculator {

    private static final Set<Double> RES = new HashSet<>();

    private Calculator() {

    }

    public static Set<Double> calculate() {
        Thread[] threads = new CalculatorWorker[MAX_THREADS];
        for (int i = 0; i < round((double) N / (double) MAX_THREADS); i++) {
            for (int j = 0; j < MAX_THREADS; j++) {
                int arg = j + i * MAX_THREADS;
                if (arg > N) {
                    return RES;
                }
                threads[j] = new CalculatorWorker(arg);
                threads[j].start();
            }
            for (int j = 0; j < MAX_THREADS; j++) {
                try {
                    threads[j].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }
        return RES;
    }

    static class CalculatorWorker extends Thread {

        private final int i;

        CalculatorWorker(int i) {
            this.i = i;
        }

        @Override
        public void run() {
            try {
                Set<Double> result = TestCalc.calculate(i);
                synchronized (RES) {
                    RES.addAll(result);
                }
            } catch (TestException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }

    }

}
